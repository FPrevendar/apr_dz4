package genetski.binarniKrizanja;

import genetski.BinarnaJedinka;

public interface IBinarniKrizanje {
	
	public BinarnaJedinka krizaj(BinarnaJedinka b1, BinarnaJedinka b2);

}
