package point;

public class Point implements IPoint {
	
	private int dimenzija;
	private double[] koordinate;
	
	
	public Point(int dimenzija) {
		this.dimenzija = dimenzija;
		koordinate = new double[dimenzija];
	}
	
	private boolean checkBounds(int d) {
		if(d < dimenzija) {
			return true;
		}else {
			return false;
		}
	}
	
	public static IPoint ParsePoint(String str) {
		IPoint point;
		try {
			String[] brojevi = str.trim().split("\\s+");
			point = new Point(brojevi.length);
			for(int i = 0; i < brojevi.length; i++) {
				point.set(i,Double.parseDouble(brojevi[i]));
			}
			}catch(Exception e) {
				throw new IllegalArgumentException("Greska pri parsiranju tocke");
			}
		return point;
	}
	
	@Override
	public int getDimenstion() {
		return dimenzija;
	}

	@Override
	public double get(int dimenzija) {
		if(checkBounds(dimenzija)) {
			return koordinate[dimenzija];
		}
		throw new IllegalArgumentException("Kriva dimenzija tocke");
	}

	@Override
	public void set(int dimenzija, double val) {
		if(checkBounds(dimenzija)) {
			koordinate[dimenzija] = val;
			return;
		}else {
			throw new IllegalArgumentException("Kriva dimenzija tocke");
		}
	}

	@Override
	public IPoint copy() {
		IPoint other = new Point(dimenzija);
		for(int i = 0; i < dimenzija; i++) {
			other.set(i, koordinate[i]);
		}
		return other;
	}

	@Override
	public IPoint add(IPoint other) {
		if(this.getDimenstion() != other.getDimenstion()) {
			throw new IllegalArgumentException("Tocke su krivih dimenzija");
		}
		IPoint tocka = new Point(dimenzija);
		for(int i = 0; i < dimenzija; i++) {
			tocka.set(i, this.get(i) + other.get(i));
		}
		return tocka;
	}

	@Override
	public IPoint sub(IPoint other) {
		if(this.getDimenstion() != other.getDimenstion()) {
			throw new IllegalArgumentException("Tocke su krivih dimenzija");
		}
		IPoint tocka = new Point(dimenzija);
		for(int i = 0; i < dimenzija; i++) {
			tocka.set(i, this.get(i) - other.get(i));
		}
		return tocka;
	}

	@Override
	public void print() {
		for(double koordinata : koordinate) {
			System.out.print(koordinata + " ");
		}
		System.out.print("\n");
	}

	@Override
	public double length() {
		double length = 0;
		for(int i = 0; i < dimenzija; i++) {
			length += Math.pow(get(i), 2);
		}
		length = Math.sqrt(length);
		return length;
	}

	@Override
	public IPoint mul(double lambda) {
		IPoint tocka = new Point(dimenzija);
		for(int i = 0; i < dimenzija; i++) {
			tocka.set(i, this.get(i) * lambda);
		}
		return tocka;
	}
	
	public static IPoint e(int dimenzija, int koordinata) {
		if(koordinata >= dimenzija) {
			throw new IllegalArgumentException("Kriva koordinata i dimenzija");
		}
		IPoint p = new Point(dimenzija);
		p.set(koordinata, 1);
		return p;
	};
	
	public static IPoint i(int dimenzija) {
		IPoint tocka = new Point(dimenzija);
		for(int i = 0; i < dimenzija; i++) {
			tocka.set(i, 1);
		}
		return tocka;
	}

}
