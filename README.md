## 4\. domaća zadaća iz analize i projektiranja računalom

U ovoj domaćoj zadaći je bilo potrebno ostvarititi postupak optimizacije koristeći genetske algoritme.
Bilo je potrebno ostvariti i izbor nekoliko parametara genetskog algoritma, to su:
* Veličina populacije
* Vjerojatnost mutacije
* Odabir operatora križanja i mutacije
* Odabir vrste prikaza rješenja (binarni ili realni broj)

To je ostvareno u programskom jeziku java, i koriste se metode ostvarene u [1\.](https://gitlab.com/FPrevendar/apr_dz1), [2\.](https://gitlab.com/FPrevendar/apr_dz2) i [3\.](https://gitlab.com/FPrevendar/apr_dz3) domaćoj zadaći.

Tekst zadatka se nalazi [ovdje](https://www.fer.unizg.hr/_download/repository/vjezba_4%5B4%5D.pdf).
