package algoritmi;

import point.IPoint;

/**
 * Staticna klasa koja predstavlja interval
 * (Samo sprema dvije double vrijednosti)
 * @author filip
 *
 */
public class Interval{
	public double left;
	public double right;
	public double fl;
	public double fr;
	public int koordinata;
	public IPoint tocka;
}