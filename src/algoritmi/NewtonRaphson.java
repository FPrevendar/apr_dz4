package algoritmi;

import funkcije.DerivableFunction;
import funkcije.FunkcijaAdapter1D;
import matrix.IMatrix;
import point.IPoint;
import point.Point;

public class NewtonRaphson {
	
	private static int MAX_DO_DIVERGENCIJE = 100;
	
	public static IPoint NewtonRaphsoinovPostupak(DerivableFunction funkcijaCilja,
			IPoint pocetnaTocka, double preciznost, boolean linijskoPretrazivanje){
		
		IPoint najboljaTocka = pocetnaTocka.copy();
		double najboljaVrijednost = funkcijaCilja.getValueAtPoint(najboljaTocka);
		int provjeraDivergencije = 0;
		double norma = 0;
		IPoint trenutnaTocka = najboljaTocka.copy();
		IMatrix gradijent = funkcijaCilja.racunajGradijent(trenutnaTocka);
		IMatrix hessian = funkcijaCilja.racunajHessiana(trenutnaTocka);
		do {
			provjeraDivergencije++;
			gradijent = funkcijaCilja.racunajGradijent(trenutnaTocka);
			hessian = funkcijaCilja.racunajHessiana(trenutnaTocka);
			/*
			System.out.println("grad");
			gradijent.print();
			System.out.println("hess");
			hessian.print();
			System.out.println("hess inverz");
			hessian.inverz().print();
			*/
			IMatrix pomak = hessian.inverz().mul(gradijent);
			norma = matrixNorm(pomak);//norma pomaka prije linijskog pretrazivanja
			
			if(linijskoPretrazivanje) {
				Interval intervalLambda = ZlatniRez.postupakZlatnogReza(new FunkcijaAdapter1D(funkcijaCilja, trenutnaTocka, pomak), Point.ParsePoint("0"));
				double lambda = (intervalLambda.left + intervalLambda.right)/2;
				//System.out.println("lambda " + lambda);
				//System.out.println();
				trenutnaTocka = trenutnaTocka.add(matrixToPoint(pomak).mul(lambda));
				norma = matrixNorm(pomak.mul(lambda)); //norma pomaka
			}else {
				trenutnaTocka = trenutnaTocka.sub(matrixToPoint(pomak));
			}
			double trenutnaVrijednost = funkcijaCilja.getValueAtPoint(trenutnaTocka);
			if(trenutnaVrijednost < najboljaVrijednost) {
				najboljaVrijednost = trenutnaVrijednost;
				provjeraDivergencije = 0;
				najboljaTocka = trenutnaTocka.copy();
			}
			//System.out.println("tocka");
			//trenutnaTocka.print();
		}while(provjeraDivergencije < MAX_DO_DIVERGENCIJE && norma > preciznost);
		
		System.out.println("Proveden je Newton Raphsonov postupak.");
		System.out.print(linijskoPretrazivanje ? "Koristeno je" : "Nije korišteno");
		System.out.println(" linijsko pretrazivanje.");
		System.out.println("Postupak je" + ((provjeraDivergencije < MAX_DO_DIVERGENCIJE) ? " konvergirao." : " divergirao."));
		System.out.print("Pronadjena je najbolja tocka: ");
		najboljaTocka.print();
		System.out.println("Vrijednost u toj tocki je " + najboljaVrijednost);
		System.out.println("Funkcija je pozvana " + funkcijaCilja.getNumberOfCalls() + " puta");
		System.out.println("Gradijent je racunat " + funkcijaCilja.getBrojRacunanjaGradijeta() + " puta");
		System.out.println("Hessian je racunat " + funkcijaCilja.getBrojRacunanjaHessiana() + " puta");
		
		return najboljaTocka;	
	}
	
	private static IPoint matrixToPoint(IMatrix m) {
		IPoint p = new Point(m.visina());
		for(int i = 0; i < m.visina(); i++) {
			p.set(i, m.get(i, 0));
		}
		return p;
	}
	
	private static double matrixNorm(IMatrix m) {
		double norm = 0;
		for(int i = 0; i < m.visina(); i++) {
			norm += Math.pow(m.get(i, 0), 2);
		}
		return Math.sqrt(norm);
	}
	
	public static IPoint NewtonRaphsoinovPostupak(DerivableFunction funkcijaCilja,
			IPoint pocetnaTocka, boolean linijskoPretrazivanje) {
		return 	NewtonRaphsoinovPostupak(funkcijaCilja, pocetnaTocka,
				10e-6, linijskoPretrazivanje);
	}

}
