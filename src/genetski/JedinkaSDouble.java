package genetski;

import java.util.Random;

import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class JedinkaSDouble implements IJedinka {
	
	private IPoint tocka;
	private DoubleJedinkaBlueprint blueprint;
	
	public JedinkaSDouble(IPoint p, DoubleJedinkaBlueprint blueprint) {
		this.blueprint = blueprint;
		tocka = p.copy();
	}

	@Override
	public IPoint dekodiraj() {
		return tocka.copy();
	}

	@Override
	public void ispisi() {
		tocka.print();
	}

	@Override
	public double ispitajDobrotu(IFunction fja) {
		return fja.getValueAtPoint(tocka);
	}

	@Override
	public DoubleJedinkaBlueprint getBlueprint() {
		return blueprint;
	}
	
	public static JedinkaSDouble stvoriRandomJedinku(DoubleJedinkaBlueprint blueprint) {
		Random rand = new Random();
		IPoint p = new Point(blueprint.getDimenzija());
		for(int i = 0; i < blueprint.getDimenzija(); i++) {
			p.set(i, blueprint.getMin().get(i) + rand.nextDouble() * blueprint.getVelicinaIntervala().get(i));
		}
		return new JedinkaSDouble(p, blueprint);
	}

}
