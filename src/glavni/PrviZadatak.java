package glavni;

import funkcije.FunkcijaBroj3;
import funkcije.IFunction;
import funkcije.RosenbrockovaBananaFunkcija;
import funkcije.SchafferovaFunkcijaF6;
import funkcije.SkoroSchafferovaFunkcijaF7;
import genetski.BinarnaJedinkaBlueprint;
import genetski.BinarniGenetskiAlgoritam;
import genetski.DoubleGenetskiAlgoritam;
import genetski.DoubleJedinkaBlueprint;
import genetski.binarniKrizanja.IBinarniKrizanje;
import genetski.binarniKrizanja.UniformnoKrizanje;
import genetski.binarniMutacija.BinarnaMutacija;
import genetski.binarniMutacija.IBinarniMutacija;
import genetski.doubleKrizanja.IDoubleKrizanje;
import genetski.doubleKrizanja.KrizanjeUprosjecivanjem;
import genetski.doubleMutacija.DoubleMutacija;
import genetski.doubleMutacija.IDoubleMutacija;
import point.Point;
/*
import genetski.binarniKrizanja.KrizanjeTockomPrelaska;
import genetski.doubleKrizanja.KrizanjeIzborom;
*/

public class PrviZadatak {

	public static void main(String[] args) {
		System.out.println("Prvi zadatak");
		
		IFunction f1 = new RosenbrockovaBananaFunkcija();
		IFunction f3 = new FunkcijaBroj3(5);
		IFunction f6 = new SchafferovaFunkcijaF6(2);
		IFunction f7 = new SkoroSchafferovaFunkcijaF7(2);
		
		BinarnaJedinkaBlueprint f167binBlprint = new BinarnaJedinkaBlueprint(Point.i(2).mul(-50),
				Point.i(2).mul(150), 0.001);
		BinarnaJedinkaBlueprint f3binBlprint = new BinarnaJedinkaBlueprint(Point.i(5).mul(-50),
				Point.i(5).mul(150), 0.001);
		
		DoubleJedinkaBlueprint f167douBlPrint = new DoubleJedinkaBlueprint(Point.i(2).mul(-50), Point.i(2).mul(150));
		DoubleJedinkaBlueprint f3douBlPrint = new DoubleJedinkaBlueprint(Point.i(5).mul(-50), Point.i(5).mul(150));
		
		
		//IBinarniKrizanje binKriz = new KrizanjeTockomPrelaska();
		IBinarniKrizanje binKrizUnif = new UniformnoKrizanje();
		IBinarniMutacija binMut = new BinarnaMutacija(0.1);
		
		IDoubleMutacija douMut = new DoubleMutacija(0.1);
		IDoubleKrizanje douKrP = new KrizanjeUprosjecivanjem();
		//IDoubleKrizanje douKrI = new KrizanjeIzborom();
		
		int velicinaPopulacije = 10;
		int brojIteracija = 10000;
		
		System.out.println("Funkcija f1(banana), binarni");
		BinarniGenetskiAlgoritam bga1 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, f167binBlprint, velicinaPopulacije, f1, true);
		bga1.populiraj();
		bga1.provediNIteracija(brojIteracija);
		bga1.ispisiNajbolji();
		System.out.println("Funkcija f1(banana), double");
		DoubleGenetskiAlgoritam dga1 = new DoubleGenetskiAlgoritam(douMut, douKrP, f167douBlPrint, velicinaPopulacije, f1, true);
		dga1.populiraj();
		dga1.provediNIteracija(brojIteracija);
		dga1.ispisiNajbolji();
		System.out.println();
		
		System.out.println("Funkcija f3(5 varijabli), binarni");
		BinarniGenetskiAlgoritam bga3 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, f3binBlprint, velicinaPopulacije, f3, true);
		bga3.populiraj();
		bga3.provediNIteracija(brojIteracija);
		bga3.ispisiNajbolji();
		System.out.println("Funkcija f3(5 vrijabli), double");
		DoubleGenetskiAlgoritam dga3 = new DoubleGenetskiAlgoritam(douMut, douKrP, f3douBlPrint, velicinaPopulacije, f3, true);
		dga3.populiraj();
		dga3.provediNIteracija(brojIteracija);
		dga3.ispisiNajbolji();
		System.out.println();
		
		System.out.println("Funkcija f6(2 varijable), binarni");
		BinarniGenetskiAlgoritam bga6 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, f167binBlprint, velicinaPopulacije, f6, true);
		bga6.populiraj();
		bga6.provediNIteracija(brojIteracija);
		bga6.ispisiNajbolji();
		System.out.println("Funkcija f6(2 varijale), double");
		DoubleGenetskiAlgoritam dga6 = new DoubleGenetskiAlgoritam(douMut, douKrP, f167douBlPrint, velicinaPopulacije, f6, true);
		dga6.populiraj();
		dga6.provediNIteracija(brojIteracija);
		dga6.ispisiNajbolji();
		System.out.println();
		
		System.out.println("Funkcija f7(2 varijable), binarni");
		BinarniGenetskiAlgoritam bga7 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, f167binBlprint, velicinaPopulacije, f7, true);
		bga7.populiraj();
		bga7.provediNIteracija(brojIteracija);
		bga7.ispisiNajbolji();
		System.out.println("Funkcija f7(2 varijale), double");
		DoubleGenetskiAlgoritam dga7 = new DoubleGenetskiAlgoritam(douMut, douKrP, f167douBlPrint, velicinaPopulacije, f7, true);
		dga7.populiraj();
		dga7.provediNIteracija(brojIteracija);
		dga7.ispisiNajbolji();
		System.out.println();
	}

}
