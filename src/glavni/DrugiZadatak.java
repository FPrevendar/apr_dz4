package glavni;

import funkcije.IFunction;
import funkcije.SchafferovaFunkcijaF6;
import funkcije.SkoroSchafferovaFunkcijaF7;
import genetski.BinarnaJedinkaBlueprint;
import genetski.BinarniGenetskiAlgoritam;
import genetski.DoubleGenetskiAlgoritam;
import genetski.DoubleJedinkaBlueprint;
import genetski.binarniKrizanja.IBinarniKrizanje;
import genetski.binarniKrizanja.UniformnoKrizanje;
import genetski.binarniMutacija.BinarnaMutacija;
import genetski.binarniMutacija.IBinarniMutacija;
import genetski.doubleKrizanja.IDoubleKrizanje;
import genetski.doubleKrizanja.KrizanjeUprosjecivanjem;
import genetski.doubleMutacija.DoubleMutacija;
import genetski.doubleMutacija.IDoubleMutacija;
import point.Point;
/*import genetski.binarniKrizanja.KrizanjeTockomPrelaska;
import genetski.doubleKrizanja.KrizanjeIzborom;*/

public class DrugiZadatak {

	public static void main(String[] args) {
		System.out.println("Drugi zadatak");
		
		int[] dimenzije = {1,3,6,10};
		
		//IBinarniKrizanje binKriz = new KrizanjeTockomPrelaska();
		IBinarniKrizanje binKrizUnif = new UniformnoKrizanje();
		IBinarniMutacija binMut = new BinarnaMutacija(0.1);
		
		IDoubleMutacija douMut = new DoubleMutacija(0.1);
		IDoubleKrizanje douKrP = new KrizanjeUprosjecivanjem();
		//IDoubleKrizanje douKrI = new KrizanjeIzborom();
		
		int velicinaPopulacije = 10;
		int brojIteracija = 10000;
		
		for(int dimenzija : dimenzije) {
			System.out.println("Funkcija f6 " + dimenzija + " varijabli, binarna:");
			IFunction f6 = new SchafferovaFunkcijaF6(dimenzija);
			BinarnaJedinkaBlueprint bin6 = new BinarnaJedinkaBlueprint(Point.i(dimenzija).mul(-50),
				Point.i(dimenzija).mul(150), 0.001);
			DoubleJedinkaBlueprint d6 = new DoubleJedinkaBlueprint(Point.i(dimenzija).mul(-50),
					Point.i(dimenzija).mul(150));
			BinarniGenetskiAlgoritam bga6 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin6, velicinaPopulacije, f6, true);
			bga6.populiraj();
			bga6.provediNIteracija(brojIteracija);
			bga6.ispisiNajbolji();
			System.out.println("double:");
			DoubleGenetskiAlgoritam dga6 = new DoubleGenetskiAlgoritam(douMut, douKrP, d6, velicinaPopulacije, f6, true);
			dga6.populiraj();
			dga6.provediNIteracija(brojIteracija);
			dga6.ispisiNajbolji();
			System.out.println();	
		}
		
		for(int dimenzija : dimenzije) {
			System.out.println("Funkcija f7 " + dimenzija + " varijabli, binarna:");
			IFunction f6 = new SkoroSchafferovaFunkcijaF7(dimenzija);
			BinarnaJedinkaBlueprint bin7 = new BinarnaJedinkaBlueprint(Point.i(dimenzija).mul(-50),
				Point.i(dimenzija).mul(150), 0.001);
			DoubleJedinkaBlueprint d7 = new DoubleJedinkaBlueprint(Point.i(dimenzija).mul(-50),
					Point.i(dimenzija).mul(150));
			BinarniGenetskiAlgoritam bga7 = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin7, velicinaPopulacije, f6, true);
			bga7.populiraj();
			bga7.provediNIteracija(brojIteracija);
			bga7.ispisiNajbolji();
			System.out.println("double:");
			DoubleGenetskiAlgoritam dga7 = new DoubleGenetskiAlgoritam(douMut, douKrP, d7, velicinaPopulacije, f6, true);
			dga7.populiraj();
			dga7.provediNIteracija(brojIteracija);
			dga7.ispisiNajbolji();
			System.out.println();	
		}
	}

}
