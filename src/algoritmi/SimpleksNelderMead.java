package algoritmi;

import funkcije.IFunction;
import point.IPoint;
import point.Point;

public class SimpleksNelderMead {
	
	private static boolean ispis = false;
	
	public static void setIspis(boolean i) {
		ispis = i;
	}

	public static IPoint SimplexPostupakPoNelderMeadu(IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			double pomak,
			double alpha,
			double beta,
			double gamma,
			double sigma,
			double preciznost) {
		
		int dimension = pocetnaTocka.getDimenstion();
		IPoint[] tockeSimpleksa = new IPoint[dimension + 1];
		double[] vrijednostiFunkcije = new double[dimension + 1];
		tockeSimpleksa[0] = pocetnaTocka.copy();
		vrijednostiFunkcije[0] = funkcijaCilja.getValueAtPoint(pocetnaTocka);
		for(int i = 0; i < dimension; i++) {
			IPoint tocka = pocetnaTocka.copy();
			tocka.set(i, pocetnaTocka.get(i) + pomak);
			tockeSimpleksa[i + 1] = tocka;
			vrijednostiFunkcije[i + 1] = funkcijaCilja.getValueAtPoint(tocka);
		}
		IPoint xc;
		double fxc;
		do {
			double min = vrijednostiFunkcije[0];
			int minIndex = 0;
			double max = vrijednostiFunkcije[0];
			int maxIndex = 0;
			for(int i = 0; i < vrijednostiFunkcije.length; i++) {
				if(vrijednostiFunkcije[i] > max) {
					max = vrijednostiFunkcije[i];
					maxIndex = i;
				}
				if(vrijednostiFunkcije[i] < min) {
					min = vrijednostiFunkcije[i];
					minIndex = i;
				}
			}
			
			xc = new Point(dimension);
			for(int i = 0; i < tockeSimpleksa.length; i++) {
				if(i != maxIndex) {
					xc = xc.add(tockeSimpleksa[i]);
				}
			}
			xc = xc.mul((double)1/(tockeSimpleksa.length - 1));
			fxc = funkcijaCilja.getValueAtPoint(xc);
			if(ispis) {
				System.out.println("centroid");
				xc.print();
				System.out.println("f(xc) = " + fxc);
			}
			
			IPoint xr = refleksija(xc, tockeSimpleksa[maxIndex], alpha);
			double fxr = funkcijaCilja.getValueAtPoint(xr);
			if(fxr < vrijednostiFunkcije[minIndex]) {
				IPoint xe = ekspanzija(xr, xc, gamma);
				double fxe = funkcijaCilja.getValueAtPoint(xe);
				if(fxe < vrijednostiFunkcije[minIndex]) {
					tockeSimpleksa[maxIndex] = xe;
					vrijednostiFunkcije[maxIndex] = fxe;
				}else {
					tockeSimpleksa[maxIndex] = xr;
					vrijednostiFunkcije[maxIndex] = fxr;
				}
			}else {
				boolean veciZaSve = true;
				for(int j = 0; j < tockeSimpleksa.length; j++) {
					if( j != maxIndex && fxr < vrijednostiFunkcije[j]) {
						veciZaSve = false;
					}
				}
				if(veciZaSve) {
					if(fxr < vrijednostiFunkcije[maxIndex]) {
						tockeSimpleksa[maxIndex] = xr;
						vrijednostiFunkcije[maxIndex] = fxr;
					}
					IPoint xk = kontrakcija(xc, tockeSimpleksa[maxIndex], beta);
					double fxk = funkcijaCilja.getValueAtPoint(xk);
					if(fxk < vrijednostiFunkcije[maxIndex]) {
						tockeSimpleksa[maxIndex] = xk;
						vrijednostiFunkcije[maxIndex] = fxk;
					}else {
						IPoint xl = tockeSimpleksa[minIndex];
						for(int i = 0; i <tockeSimpleksa.length; i++) {
							tockeSimpleksa[i] = tockeSimpleksa[i].add(xl).mul((double)1/2);
							vrijednostiFunkcije[i] = funkcijaCilja.getValueAtPoint(tockeSimpleksa[i]);
						}
						
					}
				}else {
					tockeSimpleksa[maxIndex] = xr;
					vrijednostiFunkcije[maxIndex] = fxr;
				}
			}
				
		} while(!uvjetZaustavljanja(preciznost, fxc, vrijednostiFunkcije));
		
		return xc;	
	}
	
	private static IPoint ekspanzija(IPoint xr, IPoint xc, double gamma) {
		IPoint xe = xc.mul(1 - gamma).add(xr.mul( - gamma));
		return xe;
	}

	private static IPoint refleksija(IPoint xc, IPoint xh, double alpha) {
		IPoint xr = xc.mul(1 + alpha).add(xh.mul(-alpha));
		return xr;
	}
	
	private static IPoint kontrakcija(IPoint xc, IPoint xh, double beta) {
		IPoint xk = xc.mul(1 - beta).add(xh.mul(beta));
		return xk;
	}
	
	private static boolean uvjetZaustavljanja(double epsilon, double fxc, double[] vrijednosti) {
		double sum = 0;
		for(double vrijednost: vrijednosti) {
			sum += Math.pow(fxc - vrijednost, 2);
		}
		sum /= vrijednosti.length;
		sum = Math.sqrt(sum);
		return sum < epsilon;
	}
	
	public static IPoint SimplexPostupakPoNelderMeadu(IFunction funkcijaCilja, IPoint pocetnaTocka) {
		return SimplexPostupakPoNelderMeadu(funkcijaCilja, pocetnaTocka, 1, 1, 0.5, 2, 0.5, 10e-6);
	}
	
}
