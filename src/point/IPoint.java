package point;

/**
 * 
 * Sucelje koje predstavlja tocku
 * @author filip
 *
 */
public interface IPoint {
	
	public int getDimenstion();
	
	public double get(int dimenzija);
	
	public void set(int dimenzija, double val);
	
	public IPoint copy();
	
	public IPoint add(IPoint other);
	
	public IPoint sub(IPoint other);
	
	public IPoint mul(double lambda);
	
	public void print();
	
	public double length();
	
}
