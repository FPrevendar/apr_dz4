package funkcije;

import matrix.IMatrix;
import point.IPoint;

public interface DerivableFunction extends IFunction {
	
	public IMatrix racunajGradijent(IPoint tocka);
	
	public IMatrix racunajHessiana(IPoint tocka);

	public int getBrojRacunanjaHessiana();

	public int getBrojRacunanjaGradijeta();

}
