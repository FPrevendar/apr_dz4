package ogranicenja;

import point.IPoint;

public interface IOgranicenje {
	
	public boolean jeLiZadovoljeno(IPoint tocka);
	
	public boolean checkDimension(IPoint tocka);

}
