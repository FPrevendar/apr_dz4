package genetski;

import point.IPoint;

public class BinarnaJedinkaBlueprint implements JedinkaBlueprint {

	private int dimenzija;
	private IPoint min;
	private IPoint max;
	private double preciznost;
	private IPoint velicinaIntervala;
	private int[] brojBitovaPoDimenziji;
	private int[] offset;
	private int duljina;
	
	public BinarnaJedinkaBlueprint(IPoint min, IPoint max, double preciznost) {
		this.dimenzija = min.getDimenstion();
		this.min = min.copy();
		this.max = max.copy();
		this.preciznost = preciznost;
		this.velicinaIntervala = max.sub(min);
		this.brojBitovaPoDimenziji = new int[dimenzija];
		this.offset = new int[dimenzija];
		this.duljina = 0;
		
		for(int i = 0; i < dimenzija; i++) {
			int brojVrijednosti = (int)Math.ceil(velicinaIntervala.get(i) / preciznost);
			int brojBitova = 1;
			while(Math.pow(2, brojBitova) - 1 < brojVrijednosti) {
				brojBitova++;
			}
			brojBitovaPoDimenziji[i] = brojBitova;
			duljina = getDuljina() + brojBitova;
			if(i == 0) {
				offset[i] = 0;
			}else {
				offset[i] = offset[i - 1] + brojBitovaPoDimenziji[i - 1];
			}
		}
	}

	public int getDuljina() {
		return duljina;
	}
	
	@Override
	public IPoint getMin() {
		return min.copy();
	}

	@Override
	public IPoint getMax() {
		return max.copy();
	}

	public double getPreciznost() {
		return preciznost;
	}

	public IPoint getVelicinaIntervala() {
		return velicinaIntervala.copy();
	}

	public int[] getBrojBitovaPoDimenziji() {
		return brojBitovaPoDimenziji;
	}

	public int[] getOffset() {
		return offset;
	}
	
	@Override
	public int getDimenzija() {
		return dimenzija;
	}
}
