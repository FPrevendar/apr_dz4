package glavni;

import funkcije.IFunction;
import funkcije.SchafferovaFunkcijaF6;
import funkcije.SkoroSchafferovaFunkcijaF7;
import genetski.BinarnaJedinkaBlueprint;
import genetski.BinarniGenetskiAlgoritam;
import genetski.DoubleGenetskiAlgoritam;
import genetski.DoubleJedinkaBlueprint;
import genetski.binarniKrizanja.IBinarniKrizanje;
import genetski.binarniKrizanja.UniformnoKrizanje;
import genetski.binarniMutacija.BinarnaMutacija;
import genetski.binarniMutacija.IBinarniMutacija;
import genetski.doubleKrizanja.IDoubleKrizanje;
import genetski.doubleKrizanja.KrizanjeUprosjecivanjem;
import genetski.doubleMutacija.DoubleMutacija;
import genetski.doubleMutacija.IDoubleMutacija;
import point.Point;

public class TreciZadatak {

	public static void main(String[] args) {
		System.out.println("Treci zadatak");
		
		IBinarniKrizanje binKrizUnif = new UniformnoKrizanje();
		IBinarniMutacija binMut = new BinarnaMutacija(0.1);
		
		IDoubleMutacija douMut = new DoubleMutacija(0.1);
		IDoubleKrizanje douKrP = new KrizanjeUprosjecivanjem();
		
		int velicinaPopulacije = 10;
		int brojIteracija = 10000;
		
		int brojPonavljanja = 30;
		
		double[] f6bin3d = new double[brojPonavljanja];
		double[] f6dou3d = new double[brojPonavljanja];
		double[] f6bin6d = new double[brojPonavljanja];
		double[] f6dou6d = new double[brojPonavljanja];
		
		double[] f7bin3d = new double[brojPonavljanja];
		double[] f7dou3d = new double[brojPonavljanja];
		double[] f7bin6d = new double[brojPonavljanja];
		double[] f7dou6d = new double[brojPonavljanja];

		IFunction funkcija = new SchafferovaFunkcijaF6(3);

		BinarnaJedinkaBlueprint bin = new BinarnaJedinkaBlueprint(Point.i(3).mul(-10),
				Point.i(3).mul(10), 0.0001);
		DoubleJedinkaBlueprint dou = new DoubleJedinkaBlueprint(Point.i(3).mul(-10),
				Point.i(3).mul(10));
		for(int i = 0; i < brojPonavljanja; i++) {
			BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, velicinaPopulacije, funkcija, true);
			bga.populiraj();
			bga.provediNIteracija(brojIteracija);
			f6bin3d[i] = bga.getNajbolja().ispitajDobrotu(funkcija);
			DoubleGenetskiAlgoritam dga = new DoubleGenetskiAlgoritam(douMut, douKrP, dou, velicinaPopulacije, funkcija, true);
			dga.populiraj();
			dga.provediNIteracija(brojIteracija);
			f6dou3d[i] = dga.getNajbolja().ispitajDobrotu(funkcija);
		}
		
		funkcija = new SchafferovaFunkcijaF6(6);

		bin = new BinarnaJedinkaBlueprint(Point.i(6).mul(-10),
				Point.i(6).mul(10), 0.0001);
		dou = new DoubleJedinkaBlueprint(Point.i(6).mul(-10),
				Point.i(6).mul(10));
		for(int i = 0; i < brojPonavljanja; i++) {
			BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, velicinaPopulacije, funkcija, true);
			bga.populiraj();
			bga.provediNIteracija(brojIteracija);
			f6bin6d[i] = bga.getNajbolja().ispitajDobrotu(funkcija);
			DoubleGenetskiAlgoritam dga = new DoubleGenetskiAlgoritam(douMut, douKrP, dou, velicinaPopulacije, funkcija, true);
			dga.populiraj();
			dga.provediNIteracija(brojIteracija);
			f6dou6d[i] = dga.getNajbolja().ispitajDobrotu(funkcija);
		}
		//f7
		funkcija = new SkoroSchafferovaFunkcijaF7(3);

		bin = new BinarnaJedinkaBlueprint(Point.i(3).mul(-10),
				Point.i(3).mul(10), 0.0001);
		dou = new DoubleJedinkaBlueprint(Point.i(3).mul(-10),
				Point.i(3).mul(10));
		for(int i = 0; i < brojPonavljanja; i++) {
			BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, velicinaPopulacije, funkcija, true);
			bga.populiraj();
			bga.provediNIteracija(brojIteracija);
			f7bin3d[i] = bga.getNajbolja().ispitajDobrotu(funkcija);
			DoubleGenetskiAlgoritam dga = new DoubleGenetskiAlgoritam(douMut, douKrP, dou, velicinaPopulacije, funkcija, true);
			dga.populiraj();
			dga.provediNIteracija(brojIteracija);
			f7dou3d[i] = dga.getNajbolja().ispitajDobrotu(funkcija);
		}
		
		funkcija = new SkoroSchafferovaFunkcijaF7(6);

		bin = new BinarnaJedinkaBlueprint(Point.i(6).mul(-10),
				Point.i(6).mul(10), 0.0001);
		dou = new DoubleJedinkaBlueprint(Point.i(6).mul(-10),
				Point.i(6).mul(10));
		for(int i = 0; i < brojPonavljanja; i++) {
			BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, velicinaPopulacije, funkcija, true);
			bga.populiraj();
			bga.provediNIteracija(brojIteracija);
			f7bin6d[i] = bga.getNajbolja().ispitajDobrotu(funkcija);
			DoubleGenetskiAlgoritam dga = new DoubleGenetskiAlgoritam(douMut, douKrP, dou, velicinaPopulacije, funkcija, true);
			dga.populiraj();
			dga.provediNIteracija(brojIteracija);
			f7dou6d[i] = dga.getNajbolja().ispitajDobrotu(funkcija);
		}
		
		System.out.println("F6(3)bin, F6(6)bin, F6(3)double, F6(6)double");
		for(int i = 0; i  < brojPonavljanja; i++) {
			System.out.println(f6bin3d[i] + ", " + f6bin6d[i] + ", " + f6dou3d[i] + ", " + f6dou6d[i]);
		}
		
		System.out.println("F7(3)bin, F7(6)bin, F7(3)double, F7(6)double");
		for(int i = 0; i  < brojPonavljanja; i++) {
			System.out.println(f7bin3d[i] + ", " + f7bin6d[i] + ", " + f7dou3d[i] + ", " + f7dou6d[i]);
		}
		
		System.out.println();
	}

}
