package genetski.doubleKrizanja;

import genetski.JedinkaSDouble;

public interface IDoubleKrizanje {
	
	public JedinkaSDouble krizaj(JedinkaSDouble prva, JedinkaSDouble druga);

}
