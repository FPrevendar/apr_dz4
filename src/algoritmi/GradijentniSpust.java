package algoritmi;

import funkcije.DerivableFunction;
import funkcije.FunkcijaAdapter1D;
import matrix.IMatrix;
import point.IPoint;
import point.Point;

public class GradijentniSpust {
	
	private static int MAX_DO_DIVERGENCIJE = 100;

	public static IPoint PostupakGradijetnogSpusta(DerivableFunction funkcijaCilja,
			IPoint pocetnaTocka,
			double preciznost,
			boolean linijskoPretrazivanje) {
		
		IPoint najboljaTocka = pocetnaTocka.copy();
		double najboljaVrijednost = funkcijaCilja.getValueAtPoint(najboljaTocka);
		int provjeraDivergencije = 0;
		double norma = 0;
		IPoint trenutnaTocka = najboljaTocka.copy();
		//trenutnaTocka.print();
		IMatrix gradijent = funkcijaCilja.racunajGradijent(trenutnaTocka);
		do {
			provjeraDivergencije++;
			gradijent = funkcijaCilja.racunajGradijent(trenutnaTocka);
			norma = matrixNorm(gradijent);
			
			if(linijskoPretrazivanje) {
				Interval intervalLambda = ZlatniRez.postupakZlatnogReza(new FunkcijaAdapter1D(funkcijaCilja, trenutnaTocka, gradijent), Point.ParsePoint("0"));
				double lambda = (intervalLambda.left + intervalLambda.right)/2;
				//System.out.println("lambda " + lambda);
				trenutnaTocka = trenutnaTocka.add(matrixToPoint(gradijent).mul(lambda));
				norma = matrixNorm(gradijent.mul(lambda));
			}else {
				trenutnaTocka = trenutnaTocka.sub(matrixToPoint(gradijent));
			}
			//trenutnaTocka.print();
			double trenutnaVrijednost = funkcijaCilja.getValueAtPoint(trenutnaTocka);
			if(trenutnaVrijednost < najboljaVrijednost) {
				najboljaVrijednost = trenutnaVrijednost;
				provjeraDivergencije = 0;
				najboljaTocka = trenutnaTocka.copy();
			}
		}while(provjeraDivergencije < MAX_DO_DIVERGENCIJE && norma > preciznost);
		
		System.out.println("Proveden je postupak najbrzeg spusta.");
		System.out.print(linijskoPretrazivanje ? "Koristeno je" : "Nije korišteno");
		System.out.println(" linijsko pretrazivanje.");
		System.out.println("Postupak je" + ((provjeraDivergencije < MAX_DO_DIVERGENCIJE) ? " konvergirao." : " divergirao."));
		if(provjeraDivergencije < MAX_DO_DIVERGENCIJE) {
			System.out.println("Pronadjena je najbolja tocka:");
			najboljaTocka.print();
			System.out.println("Vrijednost u toj tocki je " + najboljaVrijednost);
			System.out.println("Funkcija je pozvana " + funkcijaCilja.getNumberOfCalls() + " puta");
			System.out.println("Gradijent je racunat " + funkcijaCilja.getBrojRacunanjaGradijeta() + " puta");
		}
		najboljaTocka.print();
		System.out.println(najboljaVrijednost);
		return najboljaTocka;
		
	}
	
	private static IPoint matrixToPoint(IMatrix m) {
		IPoint p = new Point(m.visina());
		for(int i = 0; i < m.visina(); i++) {
			p.set(i, m.get(i, 0));
		}
		return p;
	}
	
	private static double matrixNorm(IMatrix m) {
		double norm = 0;
		for(int i = 0; i < m.visina(); i++) {
			norm += Math.pow(m.get(i, 0), 2);
		}
		return Math.sqrt(norm);
	}
	
	public static IPoint PostupakGradijetnogSpusta(DerivableFunction funkcijaCilja,
			IPoint pocetnaTocka,
			boolean linijskoPretrazivanje) {
		return PostupakGradijetnogSpusta(funkcijaCilja,
				pocetnaTocka,
				10e-6,
				linijskoPretrazivanje);
	}
}
