package funkcije;

import point.IPoint;

public class FunkcijaF1 extends AbstractDerivableFunction{
	
	public FunkcijaF1() {
		derivacije = new IFunction[2];
		derivacije[0] = new dfdx1();
		derivacije[1] = new dfdx2();
		
		drugeDerivacije = new IFunction[2][2];
		drugeDerivacije[0][0] = new d2fdx12();
		drugeDerivacije[0][1] = new d2fdx1dx2();
		drugeDerivacije[1][0] = new d2fdx1dx2();
		drugeDerivacije[1][1] = new d2fdx22();
	}

	@Override
	protected double getValue(IPoint point) {
		double x1 = point.get(0);
		double x2 = point.get(1);
		return 100 * Math.pow(x2 - Math.pow(x1, 2), 2) + Math.pow(1 - x1, 2);
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == 2) {
			return true;
		}
		return false;
	}
	
	private class dfdx1 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x1 = point.get(0);
			double x2 = point.get(1);
			return 400 * Math.pow(x1, 3) + 2 * x1 - 2 - 400 * x1 * x2;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class dfdx2 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x1 = point.get(0);
			double x2 = point.get(1);
			return 200 * x2 - 200 * Math.pow(x1, 2);
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx12 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x1 = point.get(0);
			double x2 = point.get(1);
			return 1200 * Math.pow(x1, 2) - 2 - 400 * x2;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx22 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			return 200;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
	private class d2fdx1dx2 extends AbstractFunction{

		@Override
		protected double getValue(IPoint point) {
			double x1 = point.get(0);
			return - 400 * x1;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			if(point.getDimenstion() == 2) {
				return true;
			}
			return false;
		}
		
	}
	
}
