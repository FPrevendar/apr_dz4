package genetski.doubleMutacija;

import java.util.Random;

import genetski.JedinkaSDouble;
import point.IPoint;

public class DoubleMutacija implements IDoubleMutacija{
	
	private double vjerojatnostMutacije;
	private Random rand = new Random();
	
	public DoubleMutacija(double p) {
		vjerojatnostMutacije = p;
	}

	@Override
	public JedinkaSDouble mutiraj(JedinkaSDouble jedinka) {
		IPoint p = jedinka.dekodiraj().copy();
		for(int i = 0; i < jedinka.getBlueprint().getDimenzija();  i++) {
			if(rand.nextDouble() > vjerojatnostMutacije) {
				p.set(i, jedinka.getBlueprint().getMin().get(i) + rand.nextDouble() * jedinka.getBlueprint().getVelicinaIntervala().get(i));
			}
		}
		return new JedinkaSDouble(p, jedinka.getBlueprint());
	}

}
