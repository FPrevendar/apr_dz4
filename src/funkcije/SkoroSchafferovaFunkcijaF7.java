package funkcije;

import point.IPoint;

public class SkoroSchafferovaFunkcijaF7 extends AbstractFunction {
	
	private int dimenzija;
	
	public SkoroSchafferovaFunkcijaF7(int dimenzija) {
		this.dimenzija = dimenzija;
	}

	@Override
	protected double getValue(IPoint point) {
		double sumaxi2 = 0;
		for(int i = 0; i < dimenzija; i++) {
			sumaxi2 += Math.pow(point.get(i), 2);
		}
		double fx = Math.pow(sumaxi2, 0.25);
		fx *= (1 + Math.pow(Math.sin(50 * Math.pow(sumaxi2, 0.1)), 2));
		return fx;
	}

	@Override
	protected boolean checkDimension(IPoint point) {
		if(point.getDimenstion() == dimenzija) {
			return true;
		}
		return false;
	}

}
