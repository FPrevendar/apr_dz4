package genetski.doubleMutacija;

import genetski.JedinkaSDouble;

public interface IDoubleMutacija {
	
	public JedinkaSDouble mutiraj(JedinkaSDouble jedinka);

}
