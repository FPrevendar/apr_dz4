package genetski;

import funkcije.IFunction;
import point.IPoint;

public interface IJedinka {
	
	public IPoint dekodiraj();
	
	public void ispisi();
	
	public double ispitajDobrotu(IFunction fja);
	
	public JedinkaBlueprint getBlueprint();
}
