package genetski.binarniKrizanja;

import java.util.BitSet;
import java.util.Random;

import genetski.BinarnaJedinka;

public class UniformnoKrizanje implements IBinarniKrizanje {
	
	Random rand = new Random();

	@Override
	public BinarnaJedinka krizaj(BinarnaJedinka b1, BinarnaJedinka b2) {
		int duljina = b1.getBlueprint().getDuljina();
		BitSet bitovi = new BitSet(duljina);
		for(int i = 0; i < duljina; i++) {
			if(rand.nextDouble() > 0.5) {
				bitovi.set(i, b1.getBit(i));
			}else {
				bitovi.set(i, b2.getBit(i));
			}
		}
		return new BinarnaJedinka(bitovi, b1.getBlueprint());
	}

}
