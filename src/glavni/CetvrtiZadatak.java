package glavni;

import funkcije.IFunction;
import funkcije.SchafferovaFunkcijaF6;
import genetski.BinarnaJedinkaBlueprint;
import genetski.BinarniGenetskiAlgoritam;
import genetski.binarniKrizanja.IBinarniKrizanje;
import genetski.binarniKrizanja.UniformnoKrizanje;
import genetski.binarniMutacija.BinarnaMutacija;
import genetski.binarniMutacija.IBinarniMutacija;
import point.Point;

public class CetvrtiZadatak {

	public static void main(String[] args) {
		System.out.println("Cetvrti zadatak");
		IFunction funkcija = new SchafferovaFunkcijaF6(3);
		int[] velicinePopulacija = {30, 50, 100, 200};
		double[] vjerojatnostiMutacije = {0.5, 0.1, 0.05, 0.01};
		int brojPonavljanja = 30;
		int brojIteracija = 10000;
		
		double[][] dobrote = new double[4][];
		double[][] dobrote2 = new double[4][];
		
		IBinarniKrizanje binKrizUnif = new UniformnoKrizanje();
		IBinarniMutacija binMut = new BinarnaMutacija(0.1);
		BinarnaJedinkaBlueprint bin = new BinarnaJedinkaBlueprint(Point.i(3).mul(-10),
				Point.i(3).mul(10), 0.0001);
		
		for(int i = 0; i < 4; i++) {
			dobrote[i] = new double[brojPonavljanja];
			for(int j = 0; j < brojPonavljanja; j++) {
				BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, velicinePopulacija[i], funkcija, true);
				bga.populiraj();

				bga.provediNIteracija(brojIteracija);
				dobrote[i][j] = bga.getNajbolja().ispitajDobrotu(funkcija);
			}			
		}
		
		System.out.println("VelPop 30, VelPop 50, VelPop 100, VelPop 200");
		for(int j = 0; j < brojPonavljanja; j++) {
			System.out.println(dobrote[0][j] + ", " + dobrote[1][j] + ", " + dobrote[2][j] + ", " + dobrote[3][j]);
		}
		
		for(int i = 0; i < 4; i++) {
			dobrote2[i] = new double[brojPonavljanja];
			for(int j = 0; j < brojPonavljanja; j++) {
				binMut = new BinarnaMutacija(vjerojatnostiMutacije[i]);
				BinarniGenetskiAlgoritam bga = new BinarniGenetskiAlgoritam(binMut, binKrizUnif, bin, 50, funkcija, true);
				bga.populiraj();

				bga.provediNIteracija(brojIteracija);
				dobrote2[i][j] = bga.getNajbolja().ispitajDobrotu(funkcija);
			}			
		}
		
		System.out.println("Mutacija 0.5, Mutacija 0.1, Mutacija 0.05, Mutacija 0.01");
		for(int j = 0; j < brojPonavljanja; j++) {
			System.out.println(dobrote2[0][j] + ", " + dobrote2[1][j] + ", " + dobrote2[2][j] + ", " + dobrote2[3][j]);
		}
		
		System.out.println();
	}

}
