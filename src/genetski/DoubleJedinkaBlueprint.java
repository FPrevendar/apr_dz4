package genetski;

import point.IPoint;

public class DoubleJedinkaBlueprint implements JedinkaBlueprint {
	
	private IPoint min;
	private IPoint max;
	private int dimenzija;
	private IPoint velicinaIntervala;
	
	
	public DoubleJedinkaBlueprint(IPoint min, IPoint max) {
		this.dimenzija = min.getDimenstion();
		this.min = min.copy();
		this.max = max.copy();
		this.velicinaIntervala = max.sub(min);
	}

	@Override
	public IPoint getMin() {
		return min.copy();
	}

	@Override
	public IPoint getMax() {
		return max.copy();
	}

	@Override
	public int getDimenzija() {
		return dimenzija;
	}
	
	public IPoint getVelicinaIntervala() {
		return velicinaIntervala.copy();
	}
}
