package genetski;

import point.IPoint;

public interface JedinkaBlueprint {

	public IPoint getMin();

	public IPoint getMax();

	public int getDimenzija();
	
	public IPoint getVelicinaIntervala();

}