package algoritmi;

import funkcije.AbstractFunction;
import funkcije.IFunction;
import ogranicenja.*;
import point.IPoint;

public class TransformacijaUProblemBezOgranicenja {
	
	public enum PostupakZaPrimjenu{
		HOOKE_JEEVES, SIMPLEKS_NELDER_MEAD
	}
	
	private static final double NEKA_VELIKA_VRIJENDOST = 10e10;
	private static final int PROVJERA_DIVERGENCIJE = 100;

	
	public static IPoint PostupakTransformacijeUProblemBezOgranicenja(
			IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			OgranicenjeJednakosti[] jednakosti,
			OgranicenjeNejednakosti[] nejednakosti,
			PostupakZaPrimjenu postupak,
			double preciznost,
			double t) {
		double ti = t;
		IPoint tocka = pocetnaTocka.copy();
		
		if(!jesuZadovoljeneNejednakosti(nejednakosti, pocetnaTocka)) {
			tocka = TrazenjeUnutarnjeTocke.postupakTrazenjaUnutarnjeTocke(funkcijaCilja, tocka, nejednakosti, postupak);
			/*System.out.println("Trazim");
			tocka.print();*/
		}
		
		IFunction funkcijaU = new FunkcijaU(funkcijaCilja, jednakosti, nejednakosti, t);
		IPoint novaTocka;
		if(postupak == PostupakZaPrimjenu.HOOKE_JEEVES) {
			novaTocka = HookeJeeves.HookeJeevesPostupak(funkcijaU, tocka);
			//novaTocka.print();
		}else {
			novaTocka = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(funkcijaU, tocka);
		}
		
		double najboljaVrijednost = funkcijaCilja.getValueAtPoint(novaTocka);
		int iteracijaBezPoboljsanja = 0;
		
		
		while(zadovoljenUvjet(tocka, novaTocka, preciznost)) {
			ti = ti * 10;
			tocka = novaTocka;
			funkcijaU = new FunkcijaU(funkcijaCilja, jednakosti, nejednakosti, ti);
			if(postupak == PostupakZaPrimjenu.HOOKE_JEEVES) {
				novaTocka = HookeJeeves.HookeJeevesPostupak(funkcijaU, tocka);
			}else {
				novaTocka = SimpleksNelderMead.SimplexPostupakPoNelderMeadu(funkcijaU, tocka);
			}
			
			if(najboljaVrijednost < funkcijaCilja.getValueAtPoint(novaTocka)) {
				iteracijaBezPoboljsanja++;
				if(iteracijaBezPoboljsanja >= PROVJERA_DIVERGENCIJE) {
					break;
				}
			}else {
				najboljaVrijednost = funkcijaCilja.getValueAtPoint(novaTocka);
			}
			
		};
		return tocka;
	}
	
	private static boolean jesuZadovoljeneNejednakosti(
			OgranicenjeNejednakosti[] nejednakosti,
			IPoint tocka) {
		
		for(OgranicenjeNejednakosti nejed: nejednakosti) {
			if(!(nejed.vrijednostOgranicenja(tocka) > 0)) {
				return false;
			}
		}
		return true;
	}
	
	private static class FunkcijaU extends AbstractFunction{
		
		private IFunction fja;
		private OgranicenjeJednakosti[] jednakosti;
		private OgranicenjeNejednakosti[] nejednakosti;
		private double t;
		
		public FunkcijaU(IFunction originalnaFunkcija, OgranicenjeJednakosti[] jed, OgranicenjeNejednakosti[] nejed, double t) {
			fja = originalnaFunkcija;
			jednakosti = jed;
			nejednakosti = nejed;
			this.t = t;
		}

		@Override
		protected double getValue(IPoint point) {
			double vrijednost = fja.getValueAtPoint(point);
			for(OgranicenjeNejednakosti nejed: nejednakosti) {
				if(nejed.vrijednostOgranicenja(point) > 0) {
					vrijednost -= (double)1/t * Math.log(nejed.vrijednostOgranicenja(point));
				}else if(nejed.vrijednostOgranicenja(point) < 0){
					return NEKA_VELIKA_VRIJENDOST;
				}else if(nejed.vrijednostOgranicenja(point) == 0) {
					vrijednost -= (double)1/t * Math.log(10e-6);// da izbjegnem log(0)
				}
			}
			for(OgranicenjeJednakosti jed : jednakosti) {
				vrijednost += t * Math.pow(jed.vrijednostOgranicenja(point), 2);
			}
			return vrijednost;
		}

		@Override
		protected boolean checkDimension(IPoint point) {
			return true;
		}
	}
	
	private static boolean zadovoljenUvjet(IPoint p1, IPoint p2, double preciznost) {
		for(int i = 0; i < p1.getDimenstion(); i++) {
			if(Math.abs(p1.get(i) - p2.get(i)) > preciznost) {
				return true;
			}
		}
		return false;
	}
	
	public static IPoint PostupakTransformacijeUProblemBezOgranicenja(
			IFunction funkcijaCilja,
			IPoint pocetnaTocka,
			OgranicenjeJednakosti[] jednakosti,
			OgranicenjeNejednakosti[] nejednakosti,
			PostupakZaPrimjenu postupak) {
		return PostupakTransformacijeUProblemBezOgranicenja(
				funkcijaCilja,
				pocetnaTocka,
				jednakosti,
				nejednakosti,
				postupak,
				10e-6,
				1);
	}
}
