package ogranicenja;

import point.IPoint;

public abstract class OgranicenjeNejednakosti implements IOgranicenje {
	
	private int dimenzija;
	
	public OgranicenjeNejednakosti(int dimenzija) {
		this.dimenzija = dimenzija;
	}
	
	public abstract double vrijednostOgranicenja(IPoint tocka);

	@Override
	public boolean jeLiZadovoljeno(IPoint tocka) {
		if(vrijednostOgranicenja(tocka) >= 0) {
			return true;
		}
		return false;
	}

	@Override
	public boolean checkDimension(IPoint tocka) {
		if(tocka.getDimenstion() == dimenzija) {
			return true;
		}
		return false;
	}
	
}
